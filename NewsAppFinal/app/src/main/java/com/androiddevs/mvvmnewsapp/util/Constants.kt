package com.androiddevs.mvvmnewsapp.ui.util

class Constants {

    companion object{
        const val API_KEY = "38beb7a3629f42629e8f9ed4a32a336c"
        const val BASE_URL = "https://newsapi.org"
        const val SEARCH_NEWS_TIME_DELAY = 500L
    }
}